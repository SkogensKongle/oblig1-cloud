package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

//For sending it back as json.
type SendingJson struct {
	Name      string   `json:"project"`
	Uname     string   `json:"owner"`
	Tuname    string   `json:"committer"`
	Commit    int      `json:"commits"`
	Languages []string `json:"language"`
}

//For getting what i need from the repo site.
type FromGithub struct {
	FullName string `json:"full_name"`
	Owner    struct {
		Username string `json:"login"`
	} `json:"owner"`
}

//For getting what i need from the repo contributions site.
type FromContributors struct {
	ToppUsername string `json:"login"`
	Commits      int    `json:"contributions"`
}

var p FromGithub
var i []FromContributors
var y SendingJson

//Using the repo from when i did the last project in experiencedesign.
//reason? i dont normaly use github, but bitbucket, and it was the first one that came to my mind.
var url1 = "https://api.github.com/repos/Stektpotet/Amazeking"
var url2 = "https://api.github.com/repos/Stektpotet/Amazeking/contributors"
var url3 = "https://api.github.com/repos/Stektpotet/Amazeking/languages"

func main() {
	http.HandleFunc("/", handler)
	//++++ code taken from a toturial for conecting to heroku. https://mmcgrana.github.io/2012/09/getting-started-with-go-on-heroku.html. +++++
	fmt.Println("listening...")
	err := http.ListenAndServe(":"+os.Getenv("PORT"), nil)
	if err != nil {
		panic(err)
	}
	//code taken end.
}

func getName(x FromGithub) string {
	var g string
	g = "https://api.github.com/repos/" + x.FullName
	return g
}
func getUname(x FromGithub) string {
	var g string
	g = x.Owner.Username
	return g
}
func getTuname(x []FromContributors) string {
	var g string
	g = x[0].ToppUsername
	return g
}
func getCommit(x []FromContributors) int {
	var g int
	g = x[0].Commits
	return g
}

func getShit(url string) []byte {
	repo, err := http.Get(url)

	body, err := ioutil.ReadAll(repo.Body)
	if err != nil {
		panic(err)
	}
	defer repo.Body.Close()
	return body
}

func handler(w http.ResponseWriter, r *http.Request) {

	//Fetch response form url1

	repo3, err := http.Get(url3)
	if err != nil {
		panic(err)
	}
	defer repo3.Body.Close()

	//Grab body from Response

	body3, err := ioutil.ReadAll(repo3.Body)
	if err != nil {
		panic(err)
	}
	//Unmarshalling the body into a payload
	j := make(map[string]int)

	body1 := getShit(url1)
	body2 := getShit(url2)
	//-------------------- url1 --------------------------------
	err = json.Unmarshal(body1, &p)
	if err != nil {
		panic(err)
	}

	//------------------- url2 -------------------------------

	err = json.Unmarshal(body2, &i)
	if err != nil {
		panic(err)
	}

	//------------------- url3 -------------------------------

	err = json.Unmarshal(body3, &j)
	if err != nil {
		panic(err)
	}
	y.Name = getName(p)
	y.Uname = getUname(p)
	y.Tuname = getTuname(i)
	y.Commit = getCommit(i)
	for h := range j {
		y.Languages = append(y.Languages, h)
	}

	//sending it as json.
	json.NewEncoder(w).Encode(y)
}
