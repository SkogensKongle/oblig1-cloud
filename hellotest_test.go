package main

import "testing"

type TestingGithub struct {
	FuName string `json:"full_name"`
	Ower   struct {
		Usname string `json:"login"`
	} `json:"owner"`
}

func TestName(t *testing.T) {
	var x FromGithub
	x.FullName = "Stektpotet/Amazeking"
	var input = getName(x)
	var output = "https://api.github.com/repos/Stektpotet/Amazeking"

	if input != output {
		t.Errorf("ERR: Expecting %s, but got %s ", output, input)
	}
}

func TestUname(t *testing.T) {
	var x FromGithub
	x.Owner.Username = "Stektpotet"
	var input = getUname(x)
	var output = "Stektpotet"

	if input != output {
		t.Errorf("ERR: Expecting %s, but got %s ", output, input)
	}
}

func TestTuname(t *testing.T) {
	var c FromContributors
	var y []FromContributors
	y = append(y, c)
	y[0].ToppUsername = "Stektpotet"
	var input = getTuname(y)
	var output = "Stektpotet"

	if input != output {
		t.Errorf("ERR: Expecting %s, but got %s ", output, input)
	}
}

func TestCommit(t *testing.T) {
	var c FromContributors
	var y []FromContributors
	y = append(y, c)
	y[0].Commits = 107
	var input = getCommit(y)
	var output = 107

	if input != output {
		t.Errorf("ERR: Expecting %s, but got %s ", output, input)
	}
}
